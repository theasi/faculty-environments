#!/bin/bash

set -eu

echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" |
    sudo tee /etc/apt/sources.list.d/bazel.list
curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
sudo apt-get update -q
sudo apt-get install -qy openjdk-8-jdk bazel

git clone https://github.com/tensorflow/tensorflow
cd tensorflow
git checkout v1.4.0

env TEST_TMPDIR=/tmp/bazel TF_NEED_JEMALLOC=1 TF_NEED_GCP=0 TF_NEED_HDFS=0 \
    TF_NEED_S3=0 TF_ENABLE_XLA=0 TF_NEED_GDR=0 TF_NEED_VERBS=0 \
    TF_NEED_OPENCL=0 TF_NEED_CUDA=0 TF_NEED_MPI=0 \
    PYTHON_BIN_PATH=/opt/anaconda/envs/Python3/bin/python \
    PYTHON_LIB_PATH=/opt/anaconda/envs/Python3/lib/python3.6/site-packages \
    CC_OPT_FLAGS="-march=native" ./configure
env TEST_TMPDIR=/tmp/bazel bazel build --config=opt \
    //tensorflow/tools/pip_package:build_pip_package

bazel-bin/tensorflow/tools/pip_package/build_pip_package ../
cd ../
rm -fr tensorflow
pip install tensorflow-1.4.0-cp36-cp36m-linux_x86_64.whl
