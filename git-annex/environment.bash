#!/bin/bash

set -eu

# Prevent Debconf from prompting for user interaction during package
# installation.
export DEBIAN_FRONTEND=noninteractive

# Add NeuroDebian APT repository and import the signing key.
wget -qO - http://neuro.debian.net/lists/bionic.de-m.libre |
    sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
sudo apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9

# Update the APT repository cache and install the git-annex package.
sudo apt-get update -q
sudo apt-get install -qy git-annex-standalone
