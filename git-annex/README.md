# git-annex

[git-annex][git-annex] is a tool for managing files with Git, without checking
the actual contents of the file into Git. This is useful to data scientists and
machine learning engineers to version training, test, and validation datasets
when dealing with files larger than Git can natively handle.

On Faculty, the version of git-annex included in the Ubuntu 18.04 repositories
is outdated, therefore this environment downloads an [up-to-date standalone
package][neurodebian-package] maintained by the NeuroDebian project.

[git-annex]: https://git-annex.branchable.com/
[neurodebian-package]: http://neuro.debian.net/pkgs/git-annex-standalone.html
