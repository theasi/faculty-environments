# Shiny Server

[Shiny][shiny] is a web application framework for [R][r]. This environment
installs RStudio Shiny Server, and configures it to be served instead of the
default Jupyter notebook on a Faculty server.

[r]: https://www.r-project.org/
[shiny]: https://shiny.rstudio.com/
