#!/bin/bash

set -eu

# Prevent Debconf from prompting for user interaction during package
# installation.
export DEBIAN_FRONTEND=noninteractive

# Install recommended R packages from CRAN repository.
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-keys 51716619E084DAB9
echo 'deb https://cran.r-project.org/bin/linux/ubuntu bionic-cran35/' |
    sudo tee -a /etc/apt/sources.list.d/cran.list
sudo apt-get update
sudo apt-get install -y r-recommended

sudo R -e "install.packages('rmarkdown', repos='https://cran.rstudio.com/')"
sudo R -e "install.packages('shiny', repos='https://cran.rstudio.com/')"

# Install Shiny Server.
wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.9.923-amd64.deb
sudo apt-get install -y ./shiny-server-1.5.9.923-amd64.deb
rm shiny-server-1.5.9.923-amd64.deb

# Configure Shiny Server to bind to the correct address and port for
# Faculty.
sudo sed -i 's/listen 3838/listen 8888/g' /etc/shiny-server/shiny-server.conf

# Stop the running Jupyter notebook server, configure the init system to run
# Shiny Server in it's place, and start the Shiny Server service.
sudo sv stop jupyter
sudo rm -fr /etc/service/jupyter
sudo mkdir -p /etc/service/shiny-server
cat > run << EOF
#!/bin/sh
set -e
cd /project
exec /usr/bin/shiny-server
EOF

chmod 755 run
sudo mv run /etc/service/shiny-server/run
