#!/bin/bash

set -eu

# Install redis
sudo apt-get update
sudo apt-get install redis-server --yes

# Create a service for redis
cat > run << EOF
#!/bin/sh
set -e
exec redis-server >> /var/log/redis-server.log 2>&1
EOF
chmod 755 run
sudo mkdir -p /etc/service/redis
sudo mv run /etc/service/redis/run
