# Redis

[Redis][redis] is an open source (BSD licensed), in-memory data structure store,
used as a database, cache and message broker. It's of use in Data Science
applications using tools like [RQ][rq], where you need to handle longer running
tasks such as [in a Data Science API][rq api example].

## Instructions

Paste the contents of `environment.bash` into a Faculty environment. When
applied to a Faculty server, it will install and run redis in the background.

_Important:_ Bear in mind that any data stored in redis will be lost when the
Faculty server is destroyed.

[redis]: https://redis.io/
[rq]: https://python-rq.org/
[rq api example]:
  https://acroz.github.io/2018/01/07/data-science-apis-long-running-tasks/
