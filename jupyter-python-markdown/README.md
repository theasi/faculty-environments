# Python Markdown Jupyter extension

The Python Markdown extension allows displaying output produced by the currently
kernel in markdown cells. The extensions is basically agnostic to the kernel
language.

More info can be found in the
[upstream documentation](http://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/python-markdown/readme.html).
