#!/bin/bash

set -eu

# Prevent Debconf from prompting for user interaction during package
# installation.
export DEBIAN_FRONTEND=noninteractive

# Add GitHub's packagecloud hosted APT repository.
wget -qO - \
    https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh |
    sudo bash

# Install the git-lfs package.
sudo apt-get install -qy git-lfs
