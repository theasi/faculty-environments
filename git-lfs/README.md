# git-lfs

[git-lfs][git-lfs] is an open source Git extension maintained by GitHub for
versioning large files with Git. Large files in the repository are replaced with
text pointers inside the repository, while the file contents are stored on a
git-lfs compatible remote server such as [GitHub][github-git-lfs],
[Bitbucket][bitbucket-git-lfs], or a [self hosted git-lfs server][self-hosted].
This is useful to data scientists and machine learning engineers to version
training, test, and validation datasets when dealing with files larger than Git
can natively handle.

This environment configures your server to use an APT repository hosted on
[packagecloud][packagecloud] containing upstream builds of git-lfs.

[bitbucket-git-lfs]:
  https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html
[git-lfs]: https://git-lfs.github.com/
[github-git-lfs]: https://help.github.com/articles/versioning-large-files/
[packagecloud]: https://packagecloud.io/github/git-lfs
[self-hosted]:
  https://github.com/git-lfs/git-lfs/wiki/Implementations#open-source
