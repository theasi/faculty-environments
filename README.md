# Faculty environments

Want to install the latest [TensorFlow][tensorflow] release candidate or the
[fish shell][fish] on your [Faculty][faculty] server? You're in the right
place! This repository contains custom environments which can be applied to your
servers.

## Adding an environment to your project

Find the environment you want to add in this repository, and copy the contents
of `environment.bash` into a new script on the Environments tab of your project
on Faculty.

![Where to paste the script](.images/environments-page.png)

Click "SAVE" to save the environment definition. You can now apply it to any new
or existing servers in your project.

## Contributing an environment

Contributions are welcome! To share your custom environment with the Faculty
community, [fork this repository][fork], add a new subdirectory containing an
`environment.bash` file that contains the script to apply the environment, and
[open a pull request][pr].

[fork]: https://bitbucket.org/theasi/faculty-environments/fork
[pr]: https://bitbucket.org/theasi/faculty-environments/pull-requests/new
[faculty]: https://faculty.ai
[tensorflow]: https://www.tensorflow.org/
[fish]: https://fishshell.com/
