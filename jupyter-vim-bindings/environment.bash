#!/bin/bash

set -eu

cd /opt/anaconda/envs/Python3/share/jupyter/nbextensions/
git clone https://github.com/lambdalisue/jupyter-vim-binding vim_binding
jupyter nbextension enable vim_binding/vim_binding
