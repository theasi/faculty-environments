#!/bin/bash

set -eu

pip install sherlockml-dataclean
jupyter nbextension enable dataclean --py --sys-prefix
