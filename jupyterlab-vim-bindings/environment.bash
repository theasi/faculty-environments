#!/bin/bash

set -eu

jupyter labextension install jupyterlab_vim
sudo sv restart jupyter
