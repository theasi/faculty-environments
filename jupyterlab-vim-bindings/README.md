# JupyterLab Vim bindings

This environment provides Vim-like keybindings for editing cells in notebooks
with JupyterLab. An up-to-date list of the keybindings is provided in the
[upstream repository](https://github.com/jwkvam/jupyterlab_vim).
