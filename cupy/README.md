# Cupy Installer

This installs cupy for Faculty platform servers.

Cupy is a drop-in replacement for numpy enabling 
GPU acceleration of vectorised operations. This
automates the install procedure to get it working
on Faculty platform servers.