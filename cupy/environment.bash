#!/bin/bash

# This is to install cupy - the drop-in numpy replacement for GPU accelerated calculations

# install other important packages

sudo apt-get update
sudo apt-get -y install g++ freeglut3-dev build-essential libx11-dev libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev

# CUDA 9 requires gcc 6
sudo apt -y install gcc-6
sudo apt -y install g++-6

# downoad one of the "runfile (local)" installation packages from cuda toolkit archive 
wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run -P /tmp
trap "rm /tmp/cuda_9.0.176_384.81_linux-run" EXIT

# make the download file executable
chmod +x /tmp/cuda_9.0.176_384.81_linux-run 

# Navigate through the interactive install
sudo apt -y install expect
expect << DONE
set send_slow {1 .5}
set timeout -1
spawn sudo /tmp/cuda_9.0.176_384.81_linux-run --override
sleep 5
expect "*"
send -- "q"
expect "*"
send -- "accept\r"
expect "*"
send -- "y\r"
expect "*"
send -- "n\r"
expect "*"
send -- "y\r"
expect "*"
send -- "\r"
expect "*"
send -- "y\r"
expect "*"
send -- "n\r"
expect eof
DONE

# set up symlinks for gcc/g++
sudo ln -s /usr/bin/gcc-6 /usr/local/cuda/bin/gcc
sudo ln -s /usr/bin/g++-6 /usr/local/cuda/bin/g++

# setup your paths
# shellcheck disable=SC2016
echo 'export PATH=/usr/local/cuda-9.0/bin:$PATH' > /etc/faculty_environment.d/cupy.sh
# shellcheck disable=SC2016
echo 'export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64:$LD_LIBRARY_PATH' >> /etc/faculty_environment.d/cupy.sh
source /etc/faculty_environment.d/cupy.sh

sudo apt -y install nvidia-modprobe
pip install cupy-cuda90
